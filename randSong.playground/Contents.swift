import Foundation


//Assuming the data would look something like this...
//Otherwise it's not hard to take an array of items and sort it into this
//Also when generating this array it is smart to random the array of songs!
//ITS REALLY IMPORTANT TO DO THAT!
var arr = ["a":["1", "2", "3", "4"],
           "b": ["1"],
           "c":["1", "2", "3"],
           "d": [ "1", "2"]]

//Fisher-Yates for randomising songs in array
//http://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift
extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

class Song {
    var artist: String!
    var title : String!
    var position: Float!
}

var tempArray  = [Song]()
var finalArray = [Song]()

//final array builder
for (index, (artist, songs)) in arr.enumerate() {
    let randomNumber = Float(arc4random_uniform(UInt32(1000000))) / 999999
    var spacing   = Float()
    var songPosition = Float()
    
    //If there is only one song from that artist
    if songs.count < 2 {
        
        //place single song randomly in array
        songPosition   = randomNumber * 100
        
        //Build Song
        var thisSong = Song()
        thisSong.artist = artist
        thisSong.title  = songs[0]
        thisSong.position = songPosition
        
        //Add Song To array
        tempArray.append(thisSong)
        
    } else {
        //There are more than one songs from the artist in playlist
        //space semi randomly based on percentage
        //Spreading out evenly, and adds a slight random element to spacing
        spacing = Float( 100 / songs.count ) + randomNumber
        
        //Do Fisher-Yates
        var shuffSongs = songs
        shuffSongs.shuffleInPlace()
        
        //Repeat song in songs
        for song in shuffSongs {
            //Set the position of the song and add a little randomness to it
            songPosition = (songPosition + spacing)
            
            //Build Song
            var thisSong = Song()
            thisSong.artist = artist
            thisSong.title  = song
            thisSong.position = songPosition
            
            //Add Song To array
            tempArray.append(thisSong)
        }
    }
}

finalArray = tempArray.sort({ $0.position > $1.position }).reverse()

for song in finalArray {
    print("\(song.position) \(song.artist)\(song.title)")
}

